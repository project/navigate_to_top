<?php

namespace Drupal\navigate_to_top\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;


/**
 * Administration settings form.
 */
class NavigateToTopSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'navigate_to_top_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [];
  }
  
   // Render uploaded image if available.
   function file_create_url($image_url) {
    return $image_url;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('navigate_to_top.settings');
    $settings = $config->get();

    $form['#attached']['library'][] = 'navigate_to_top/navigate_to_top';

    $form['navigate_to_top_button_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button text'),
      '#description' => $this->t('Set the text of the Navigate To Top button'),
      '#default_value' => $settings['navigate_to_top_button_text'] ?? $this->t('Navigate to top'), // Set a default value if not provided in configuration settings
      '#size' => 30,
      '#maxlength' => 30,
      '#attributes' => [
        'class' => ['navigate-to-top-button'], // Add a class for styling
      ],
    ];
     
    $form['navigate_to_top_button_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Do you want Navigate To Top to use a PNG-24 image or a Text/Css button?'),
      '#options' => [
        'image' => $this->t('Image'),
        'text' => $this->t('Text/Css'),
      ],
      '#default_value' => $settings['navigate_to_top_button_type'],
    ];
    $form['image_button'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Image button settings'),
      '#collapsible' => TRUE,
      '#collapsed' => ($form['navigate_to_top_button_type']['#default_value'] == 'text' ? TRUE : FALSE),
    ];

    $form['image_button']['navigate_to_top_image'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Image'),
      '#description' => $this->t('Upload the image for the Navigate To Top button'),
      '#upload_location' => 'public://navigate_to_top_images/',
      '#upload_validators' => [
      'file_validate_extensions' => ['png jpg jpeg gif'],
      '#required' => TRUE,
      ],
    ];
   
    // Wrap Text/Css button settings in a fieldset.
    $form['text_button'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Text/Css button settings'),
      '#collapsible' => TRUE,
      '#collapsed' => ($form['navigate_to_top_button_type']['#default_value'] == 'image' ? TRUE : FALSE),
    ];
    $form['text_button']['navigate_to_top_bg_color'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Background color'),
      '#description' => $this->t('Button background color default #F7F7F7'),
      '#default_value' => $settings['navigate_to_top_bg_color'],
      '#size' => 10,
      '#maxlength' => 7,
      '#suffix' => '<div class="color-field" id="navigate_to_top_bg_color"></div>',
    ];
    $form['text_button']['navigate_to_top_border_color'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Border color'),
      '#description' => $this->t('Border color default #CCCCCC'),
      '#default_value' => $settings['navigate_to_top_border_color'],
      '#size' => 10,
      '#maxlength' => 7,
      '#suffix' => '<div class="color-field" id="navigate_to_top_border_color"></div>',
    ];
    $form['text_button']['navigate_to_top_hover_color'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hover color'),
      '#description' => $this->t('Hover color default #EEEEEE'),
      '#default_value' => $settings['navigate_to_top_hover_color'],
      '#size' => 10,
      '#maxlength' => 7,
      '#suffix' => '<div class="color-field" id="navigate_to_top_hover_color"></div>',
    ];
    $form['text_button']['navigate_to_top_text_color'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text color'),
      '#description' => $this->t('Text color default #333333'),
      '#default_value' => $settings['navigate_to_top_text_color'],
      '#size' => 10,
      '#maxlength' => 7,
      '#suffix' => '<div class="color-field" id="navigate_to_top_text_color"></div>',
    ];
    
  // Render uploaded image if available.
  if (!empty($settings['navigate_to_top_image'])) {
  $file_id = $settings['navigate_to_top_image'][0]; // Assuming single value.
  $file = \Drupal\file\Entity\File::load($file_id);
  if ($file) {
    $image_url= \Drupal::service('file_url_generator')->generateAbsoluteString($file->getFileUri());
    $form['image_button']['uploaded_image'] = [
      '#markup' => '<img src="' . $image_url . '" alt="Navigate To Top Button" />',
    ];
  }
}
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('navigate_to_top.settings');
    $form_values = $form_state->getValues();
    $config->set('navigate_to_top_button_text', $form_values['navigate_to_top_button_text'])
      ->set('navigate_to_top_button_type', $form_values['navigate_to_top_button_type'])
      ->set('navigate_to_top_image', $form_values['navigate_to_top_image'])
      ->set('navigate_to_top_bg_color', $form_values['navigate_to_top_bg_color'])
      ->set('navigate_to_top_border_color', $form_values['navigate_to_top_border_color'])
      ->set('navigate_to_top_hover_color', $form_values['navigate_to_top_hover_color'])
      ->set('navigate_to_top_text_color', $form_values['navigate_to_top_text_color'])
      ->save();
    parent::submitForm($form, $form_state);
  }
}