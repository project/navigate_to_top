# Navigate to top

## Table of contents

- Introduction
- Installation
- Configuration
- Maintainers


## Introduction

Navigate To Top adds a button that hovers in the bottom of your screen and allow 
users to smoothly scroll up the page using jQuery.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

•	Install the module.
•	Go to configuration > User interface > Navigate to top.
•	Fill details such as, Placement, button text, bgcolor, text colour, hover colour, border colour etc.
•	Click save.
•	Now you can see navigation button on your screen.


## Maintainers:

- [Rajesh Bhimani](https://www.drupal.org/u/rajesh-bhimani)

Supporting organizations:

* [Skynet Technologies USA LLC](https://www.skynettechnologies.com)
