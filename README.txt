CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Navigate to top introduces a button that floats at the bottom of your screen, 
enabling users to effortlessly ascend the page. This feature boasts a pleasant 
easing animation and also monitors user activity on the page, such as scrolling 
or mouse clicks, ensuring that any ongoing animation is promptly halted. In contrast 
to other scripts that either immobilize the screen or abruptly transport users to 
the top, Navigate to top offers a smoother, more user-friendly experience.

INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit:
https://www.drupal.org/docs/extending-drupal/installing-modules
for further information.


CONFIGURATION
-------------

There are settings for:

•	Install the module.
•	Go to configuration > User interface > Navigate to top.
•	Fill details such as, Placement, button text, bgcolor, text colour, hover colour, border colour etc.
•	Click save.
•	Now you can see navigation button on your screen.


MAINTAINERS
-----------

- [Rajesh Bhimani](https://www.drupal.org/u/rajesh-bhimani)

Supporting organizations:

* [Skynet Technologies USA LLC](https://www.skynettechnologies.com)