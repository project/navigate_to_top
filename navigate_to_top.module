<?php

/**
 * @file
 * Adds the navigate to top button.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\file\Entity\File;
/**
 * Implements hook_page_attachments().
 */
function navigate_to_top_page_attachments(array &$attachments) {
  $config = \Drupal::config('navigate_to_top.settings');
  $settings = $config->get();
  
  $file_url = "";
  if(isset($settings['navigate_to_top_image']) && !empty($settings['navigate_to_top_image']))
  {
    $file_id = $settings['navigate_to_top_image'][0]; // Assuming single value.
    $file = \Drupal\file\Entity\File::load($file_id);
    if ($file) {
      $file_url= \Drupal::service('file_url_generator')->generateAbsoluteString($file->getFileUri());
    }
  }

  $button_settings = [
    'navigate_to_top_button_type' => $settings['navigate_to_top_button_type'],
    'navigate_to_top_button_text' => $settings['navigate_to_top_button_text'],
    'image_path' => $file_url
  ];
 
  $attachments['#attached']['library'][] = 'navigate_to_top/navigate_to_top_js';

  // Add stylesheet for image or text/css button.
  if (($settings['navigate_to_top_button_type']) == "text") {
    $attachments['#attached']['library'][] = 'navigate_to_top/navigate_to_top_text';
  }
  else {
    $attachments['#attached']['library'][] = 'navigate_to_top/navigate_to_top_image';
  }

  $css = '';
  $hover_css = '';
 
  // Check variables and add color from settings
  if (($settings['navigate_to_top_button_type'] == "text") && ($settings['navigate_to_top_bg_color'] !== '#F7F7F7')) {
    $css .= "background: " . $settings['navigate_to_top_bg_color'] . ";";
  }
  if (($settings['navigate_to_top_button_type'] == "text") && ($settings['navigate_to_top_border_color'] !== '#CCCCCC')) {
    $css .= "border-color: " . $settings['navigate_to_top_border_color'] . ";";
  }
  if (($settings['navigate_to_top_button_type'] == "text") && ($settings['navigate_to_top_hover_color'] !== '#EEEEEE')) {
    $hover_css .= "body #backtotop:hover { background: " . $settings['navigate_to_top_hover_color'] . "; border-color: " . $settings['navigate_to_top_hover_color'] . "; }";
  }
  if (($settings['navigate_to_top_button_type'] == "text") && ($settings['navigate_to_top_text_color'] !== '#333333')) {
    $css .= "color: " . $settings['navigate_to_top_text_color'] . ";";
  }

  if ($css != '') {
    $attachments['#attached']['html_head'][] = [
      [ 
        '#tag' => 'style',
        '#value' => 'body #navigatetotop {' . $css . '}' . $hover_css,
      ],
      'css',
    ];
  }
  if (!isset($attachments['#attached']['drupalSettings']['navigate_to_top'])) {
    $attachments['#attached']['drupalSettings']['navigate_to_top'] = [];
  }
  $attachments['#attached']['drupalSettings']['navigate_to_top'] += $button_settings;
}

/**
 * Implements hook_help().
 */
function navigate_to_top_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.navigate_to_top':
      $text = file_get_contents(dirname(__FILE__) . "/README.txt");
      if (!\Drupal::moduleHandler()->moduleExists('markdown')) {
        return '<pre>' . $text . '</pre>';
      }
      else {
        // Use the Markdown filter to render the README.
        $filter_manager = \Drupal::service('plugin.manager.filter');
        $settings = \Drupal::configFactory()->get('markdown.settings')->getRawData();
        $config = ['settings' => $settings];
        $filter = $filter_manager->createInstance('markdown', $config);
        return $filter->process($text, 'en');
      }
  }
  return NULL;
}